/* Webpack 設定檔 */

const path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: {
    plugs: [
      './src/js/plugs.js',
      './src/js/newsticker/jquery.newsTicker.min.js',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: '[name].min.js',
  },
  resolve: {
    // alias: {
    //   jquery: path.join(__dirname, '/src/js/jquery-3.1.1.min.js'),
    // },
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
  ],
}
