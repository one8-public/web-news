## 狼訊新聞站 Web-HTML

開發環境 ：

- tailwind css 2.0.2

- `純靜態HTML切版。`

## tailwindcss 資訊

<p>
    <a href="https://tailwindcss.com/" target="_blank">
      <img alt="Tailwind CSS" width="350" src="https://refactoringui.nyc3.cdn.digitaloceanspaces.com/tailwind-logo.svg">
    </a><br>
    A utility-first CSS framework for rapidly building custom user interfaces.
</p>

---

## Developing

1. 安裝依賴 - Install dependencies

```bash
npm install
```

2. 開發編譯 - Start local compile

```bash
npm run dev
```

3. 發佈編譯 - Start build compile

```bash
npm run build

or

NODE_ENV=production npx tailwindcss-cli@latest build ./src/css/style.css -o ./dist/css/style.css && webpack --mode productio
```
