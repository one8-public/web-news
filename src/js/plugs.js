import 'jquery'
import Swiper from 'swiper'

$(function () {
  // Create Responsive Menu
  const menu = $('nav > ul').clone(true)
  const container = $('#responsive-nav')
  container.append(menu)

  if ($('.nav-btn').length > 0) {
    $('.nav-btn').on('click', function () {
      let $this = $(this)
      checkNav($this)
    })
  }

  // 跑馬燈
  if ($('#newsticker ul').length) {
    $('#newsticker ul').newsTicker({
      row_height: 40,
      max_rows: 1,
      duration: 4000,
      effect: 'right',
      prevButton: $('#nt-prev'),
      nextButton: $('#nt-next'),
    })
  }

  // 字數限制
  if ($('.ellipsis').length > 0) {
    $('.ellipsis').each(function (i) {
      var len = parseInt($(this).attr('data-len')) // 取得字數限制
      var text = $(this).text().trim()
      if (text.length > len) {
        $(this).attr('title', text)
        var newText = text.substring(0, len - 1) + '...'
        $(this).text(newText)
      }
    })
  }

  // BANNER輪播
  if ($('#slider-banner .swiper-container').length > 0) {
    const swiper = new Swiper('#slider-banner .swiper-container', {
      slidesPerView: 1,
      spaceBetween: 30,
      loop: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    })
  }

  /*滾動回TOP*/
  if ($('.gotop').length > 0) {
    $('.gotop').on('click', function () {
      $('html,body').animate(
        {
          scrollTop: 0,
        },
        1000
      )
    })
    $(window).on('scroll', function () {
      if ($(this).scrollTop() > 300) {
        $('.gotop').fadeIn('fast')
      } else {
        $('.gotop').stop().fadeOut('fast')
      }
    })
  }
})

function checkNav(object) {
  if (object.hasClass('not-active')) {
    $('#responsive-nav')
      .find('.nav-main')
      .slideDown(300, function () {
        object.removeClass('not-active')
      })
  } else {
    $('#responsive-nav')
      .find('.nav-main')
      .slideUp(300, function () {
        object.addClass('not-active')
      })
  }
}
